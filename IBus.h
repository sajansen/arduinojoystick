//
// Created by prive on 12/9/19.
//

#ifndef JOYSTICK_IBUS_H
#define JOYSTICK_IBUS_H


#include "Arduino.h"
#pragma once

class IBus {
private:
    int len;
    int checksum;
public:
    IBus(int numChannels);

    void begin();

    void end();

    void write(unsigned short);

};


#endif //JOYSTICK_IBUS_H
