#include <Arduino.h>
#include <Wire.h>
#include <limits.h>
#include "IBus.h"
#include "MPU6050.h"

#define UPDATE_INTERVAL 20 // milliseconds
#define JOYSTICK_CHANNELS 10
#define SAMPLE_RATE 10
#define BAUD_RATE 115200
#define MPU_I2C_ADDRESS 0x68


IBus ibus(JOYSTICK_CHANNELS);

MPU6050 accelgyro = MPU6050(MPU_I2C_ADDRESS);

uint16_t axisReadings[JOYSTICK_CHANNELS] = {0};

int16_t ax, ay, az;
int32_t axa, aya, aza;
int16_t gx, gy, gz;
int32_t gxa, gya, gza;
int16_t mx, my, mz;
int32_t mxa, mya, mza;
int16_t temp;
int32_t tempa;

uint16_t int16ToUint16(int16_t value) {
    return value + SHRT_MAX;
}

void collectData() {
    axa = 0;
    aya = 0;
    aza = 0;

    gxa = 0;
    gya = 0;
    gza = 0;

    mxa = 0;
    mya = 0;
    mza = 0;

    tempa = 0;

    for (int i = 0; i < SAMPLE_RATE; i++) {
        accelgyro.getMotion9(&ax, &ay, &az,
                             &gx, &gy, &gz,
                             &mx, &my, &mz);
        axa += ax;
        aya += ay;
        aza += az;

        gxa += gx;
        gya += gy;
        gza += gz;

        mxa += mx;
        mya += my;
        mza += mz;

        tempa += accelgyro.getTemperature();
    }

    ax = (axa / SAMPLE_RATE);
    ay = (aya / SAMPLE_RATE);
    az = (aza / SAMPLE_RATE);

    gx = (gxa / SAMPLE_RATE);
    gy = (gya / SAMPLE_RATE);
    gz = (gza / SAMPLE_RATE);

    mx = (mxa / SAMPLE_RATE);
    my = (mya / SAMPLE_RATE);
    mz = (mza / SAMPLE_RATE);

    temp = (tempa / SAMPLE_RATE);

    axisReadings[0] = int16ToUint16(ax);
    axisReadings[1] = int16ToUint16(ay);
    axisReadings[2] = int16ToUint16(az);
    axisReadings[3] = int16ToUint16(gx);
    axisReadings[4] = int16ToUint16(gy);
    axisReadings[5] = int16ToUint16(gz);
    axisReadings[6] = int16ToUint16(mx);
    axisReadings[7] = int16ToUint16(my);
    axisReadings[8] = int16ToUint16(mz);
    axisReadings[9] = int16ToUint16(temp);
}

void sendData() {
    ibus.begin();

    for (uint16_t axisReading : axisReadings) {
        ibus.write(axisReading);
    }

//    // read digital pins - one per channel
//    for(i=0; i < NUM_DIGITAL_INPUTS; i++)
//        ibus.write(digitalRead(digitalPins[i]) == HIGH ? 1023 : 0);
//
//    // read digital bit-mapped pins - 16 pins go in one channel
//    for(i=0; i < NUM_DIGITAL_BITMAPPED_INPUTS; i++) {
//        int bit = i%16;
//        if(digitalRead(digitalBitmappedPins[i]) == HIGH)
//            bm_ch |= 1 << bit;
//
//        if(bit == 15 || i == NUM_DIGITAL_BITMAPPED_INPUTS-1) {
//            // data for one channel ready
//            ibus.write(bm_ch);
//            bm_ch = 0;
//        }
//    }

    ibus.end();
}

void loop() {
    static unsigned long nextUpdateTime = 0;
    if (millis() < nextUpdateTime) {
        return;
    }
    nextUpdateTime = millis() + UPDATE_INTERVAL;

    collectData();

    sendData();
}

void setup() {
    Wire.begin();
    accelgyro.initialize();
    Serial.begin(BAUD_RATE);
    Serial.println("Started.");
}